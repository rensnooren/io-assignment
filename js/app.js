//Variables
let resultAmount = 20; //Allows for more results from the API
let userData = [];
let userDataFiltered = [];

//Wait until entire page is loaded to avoid errors
$(document).ready(function() {
    window.setTimeout(retrieveData, 100);
});

//Retrieve all data from Random user API
function retrieveData() {
    fetch("https://randomuser.me/api/?nat=gb&results=" + resultAmount)
        .then((results) => {
            return results.json();
        })

        .catch(function() {
            console.log("Failed to retrieve data");
            document.getElementById("user-table").innerText = "Sorry the application was unable to load data, please try again.";
        })

        .then((data) => {
            //Save data as a global variable and populates table
            if (data != null) {
                userData = data.results;
                filterData();
            }
        });
}

//function to filter userData based on given parameters
function filterData(filter, filterParameter) {
    //resets userDataFiltered
    userDataFiltered = [];
    if (filter != null && filterParameter != null) {
        for (let i = 0; i < userData.length; i++) {
            //Check if the result matches the filter and pushes to userDataFiltered
            if (userData[i][filter] === filterParameter) {
                userDataFiltered.push(userData[i]);
            }
        }
    }
    else {
        //If no filter is selected all data from the data fetch is placed in userDataFiltered
        userDataFiltered = userData;
    }
    populateTable();
}

function sortData(sort, sortParameter) {
    if (sort != null && sortParameter != null) {
        //Split sortParameter for compare function
        const parameterProperty = sortParameter.split(".");
        //Compares filtered data to sort based on direction
        userDataFiltered[sort]((a, b) => a[parameterProperty[0]][parameterProperty[1]].localeCompare(b[parameterProperty[0]][parameterProperty[1]]));

        //Changes text of dropdown button to the selected sorting method
        let direction;
        if (sort === "sort") {
            direction = "A > Z";
        }
        else {
            direction = "Z > A";
        }
        document.getElementById("sortDropdown").innerText = parameterProperty[0] + " " + parameterProperty[1] + " " + direction;
    }
    populateTable();
}

//Function to populate the user table
function populateTable() {
    //resets table to prevent duplicate content
    $("#table-content").empty();
    for (let i = 0; i < userDataFiltered.length; i++) {
        //For each result "content" is filled with data
        let content = "<tr onclick=\"userModal(" + i + ")\">" +
            "<th scope='row'>" + (i + 1) + "</th>" +
            "<td>" + userDataFiltered[i].name.first + "</td>" +
            "<td>" + userDataFiltered[i].name.last + "</td>" +
            "<td>" + userDataFiltered[i].gender + "</td>" +
            "</tr>";
        //JQuery append to add "content" in the table
        $("#table-content").append(content);
    }
}

//Function to fill a reusable modal with specified user data
function userModal(userId) {
    //Title and profile picture
    document.getElementById("userModalLabel").innerText = "Profile of " + userDataFiltered[userId].name.first;
    document.getElementById("profilePicture").src = userDataFiltered[userId].picture.large;
    //Basic info
    document.getElementById("userName").innerText = userDataFiltered[userId].name.first + " " + userDataFiltered[userId].name.last;
    document.getElementById("userAge").innerText = userDataFiltered[userId].dob.age;
    document.getElementById("userGender").innerText = userDataFiltered[userId].gender;
    //Address details
    document.getElementById("userStreet").innerText = userDataFiltered[userId].location.street.name + " " + userDataFiltered[userId].location.street.number;
    document.getElementById("userCity").innerText = userDataFiltered[userId].location.city;
    document.getElementById("userState").innerText = userDataFiltered[userId].location.state;
    document.getElementById("userPostcode").innerText = userDataFiltered[userId].location.postcode;
    //Contact details
    document.getElementById("userEmail").innerText = userDataFiltered[userId].email;
    document.getElementById("userPhone").innerText = userDataFiltered[userId].phone;
    document.getElementById("userCell").innerText = userDataFiltered[userId].cell;
    //Opens modal with user data
    $('#userModal').modal('show');
}